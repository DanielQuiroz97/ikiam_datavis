# Load needed packages
library(ggpubr) # Plot themes
library(forcats) # Categorical data
library(tidyverse)
# Import data
ufc <- read_csv("02_magnitude/data/microbial_growth.csv")

# Add UFC collumn
ufc <- ufc %>%
  # Get UFC by multipling 
  mutate(UFC = Valor * 1e7 / Sembrado) 

# Summarise data
ufc_avg <- ufc %>% group_by(Muestra) %>% 
  # Summarise data
  summarise(Avg = mean(UFC), SD = sd(UFC), N = n()) %>% 
  # Manage categorical data
  mutate(Muestra = fct_rev(factor(Muestra)))

ggplot(ufc_avg, aes(Muestra, Avg, fill = Muestra) ) + 
  geom_bar(stat = "identity", color = "black") +
  geom_errorbar(aes(ymin = Avg - SD, ymax = Avg + SD),
                width=.1,
                position=position_dodge(.9)) +
   theme_bw() + 
  scale_fill_manual(values=c("#999999", "#E69F00", "#56B4E9")) +
  ylab('UFC')


